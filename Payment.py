import webbrowser

class Payment():

	def __init__(self, amount=0):
		self.amount = amount

	def isAccpeted(self):
		if(self.amount>=500):
			#Call another webpage here : 'Payment succed'
			webbrowser.open("https://openjournalsystems.com/payment-success/", new=0, autoraise=True)
			return True
		else:
			#Call another webpage here : 'Payment failed'
			webbrowser.open("https://dribbble.com/shots/1860960-Payment-Failed", new=0, autoraise=True)
			return False